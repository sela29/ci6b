<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kategori_model extends CI_model {

    private $nama_tabel = "kategori";
    
    public function ambilData(){
        return $this->db->get("kategori")->result();

    }

    public function hapusData($ids)
    {

    $this->db->delete("kategori", array("id"=>$ids) );
    }
}
